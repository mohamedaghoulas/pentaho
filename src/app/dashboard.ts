import { Dashboard } from './dashboards_schema';
export const Dashboards: Dashboard[] = [
    { dashboard:"Dashboard_location_risk", url: "http://192.168.1.89:8080/pentaho/api/repos/%3Ahome%3Aadmin%3ADashboard_location_risk.xdash/viewer" },
    { dashboard:"Dashboard Cartographie de risque", url: "http://192.168.1.89:8080/pentaho/api/repos/%3Ahome%3Aadmin%3ADashboard%20Cartographie%20de%20risque%20de%20l'ONEA.xdash/viewer" },
    { dashboard:"Dashboard présentation de l'évolution des risques résiduels et inhérents", url: "http://192.168.1.89:8080/pentaho/api/repos/%3Ahome%3Aadmin%3ADashboard%20Pr%C3%A9sentation%20de%20l%E2%80%99%C3%A9volution%20des%20risques%20inh%C3%A9rents%20aux%20risques%20r%C3%A9siduels.xdash/viewer" },
    { dashboard:"Dashboard processus clefs de 'organisation", url: "http://192.168.1.89:8080/pentaho/api/repos/%3Ahome%3Aadmin%3ADashboard%20Processus%20clefs%20de%20l'ONEA%20et%20pr%C3%A9sentation%20des%20principaux%20risques%20majeurs%20de%20l'ONEA%20par%20Processus.xdash/viewer" },
    { dashboard:"Dashboard risque inhérents et résiduels en fonction d'impact et probabilité", url: "http://192.168.1.89:8080/pentaho/api/repos/%3Ahome%3Aadmin%3ADashboard%20risk%20inher%20et%20risk%20resid%20en%20fonction%20de%20l'impact%20et%20de%20la%20probabilit%C3%A9.xdash/viewer" },
    { dashboard:"Dashboard risque inhérent et résiduel - Total", url: "http://192.168.1.89:8080/pentaho/api/repos/%3Ahome%3Aadmin%3ADashboard%20risque%20inh%C3%A9rent%20et%20risque%20r%C3%A9siduel%20-%20Total.xdash/viewer" },
    { dashboard:"Dashboard typologie de risque", url: "http://192.168.1.89:8080/pentaho/api/repos/%3Ahome%3Aadmin%3ADashboard%20Typologie%20de%20risque.xdash/viewer" }
  ];