import { Component,ElementRef,OnInit } from '@angular/core';
import { PentahoDashboardService } from 'pentaho-dashboard';
import { Router } from '@angular/router';
import { Dashboards}  from './dashboard';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private pentahoDashboardService: PentahoDashboardService,private router: Router,
    private hostElement: ElementRef) {
      if (this.pentahoDashboardService.isNotLoggedIn()) {
        this.router.navigate(['/login']);
      }

    }
  url = "";
  value1="";
  value2="";
  text="";
  ngOnInit() {}
  param1="";
  param2="";
  param3="";
  condition:boolean;
  selectChangeHandler1 (event: any) {
    this.param1 = event.target.value;
  }
  selectOrganisation (event: any) {
    this.param2 = event.target.value;
  }
  selectLocation (event: any) {
    this.param3 = event.target.value;
  }
  
  myClickFunction(event : any){
    const iframe = this.hostElement.nativeElement.querySelector('iframe');
    this.condition=true;
    for(let i=0;i < 7;i++){
      if(Dashboards[i].dashboard==this.param1 && this.condition){
        iframe.src = Dashboards[i].url+"?organisation="+this.param2+"&city="+this.param3+"&date_debut="+this.value1+"&date_fin="+this.value2;
        this.condition=false;
        console.log(iframe.src);
      }
    }
    ;
  }
  
}
