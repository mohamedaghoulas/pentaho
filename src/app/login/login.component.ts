import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PentahoDashboardService } from 'pentaho-dashboard';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(
    private router: Router,
    private pentahoDashboardService: PentahoDashboardService) { }
  ngOnInit() {
  }
  loginMessage = '';

  

  onSubmit(
    username: string,
    password: string) {

    if (this.pentahoDashboardService.isLoggedIn()) {
      this.router.navigate(['/']);
    }
    else {
      this.pentahoDashboardService.setUsername(username);
      this.pentahoDashboardService.setPassword(password);
      this.pentahoDashboardService.logIn('/');
      if (this.pentahoDashboardService.isNotLoggedIn()) {
        this.loginMessage = 'Cannot login on Pentaho.<br/>Try to login again.';
        console.log("not logged in")
        return false;
      }
    }
  }

}
